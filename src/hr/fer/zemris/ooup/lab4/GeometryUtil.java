package hr.fer.zemris.ooup.lab4;

import hr.fer.zemris.ooup.lab4.model.Point;

public class GeometryUtil {

	public static double distanceFromPoint(Point point1, Point point2) {
		int p1x = point1.getX();
		int p1y = point1.getY();
		int p2x = point2.getX();
		int p2y = point2.getY();
		return Math.sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y));
	}

	public static double distanceFromLineSegment(Point s, Point e, Point p) {
		int A = p.getX() - s.getX();
		int B = p.getY() - s.getY();
		int C = e.getX() - s.getX();
		int D = e.getY() - s.getY();

		int dot = A * C + B * D;
		int lengthSq = C * C + D * D;
		double param = -1;
		if (lengthSq != 0) {
			param = dot / (double) lengthSq;
		}

		if (param < 0) {
			return distanceFromPoint(p, new Point(s.getX(), s.getY()));
		} else if (param > 1) {
			return distanceFromPoint(p, new Point(e.getX(), e.getY()));
		} else {
			int x = (int) (s.getX() + param * C);
			int y = (int) (s.getY() + param * D);
			return distanceFromPoint(p, new Point(x, y));
		}
	}
}