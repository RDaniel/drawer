package hr.fer.zemris.ooup.lab4;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class FileUtil {

	public static void writeLinesToFile(List<String> rows, Path filePath) {
		try (BufferedWriter writer = Files.newBufferedWriter(filePath)) {
			for (String row : rows) {
				writer.write(row + System.lineSeparator());
			}

			writer.flush();
		} catch (IOException ignored) {
		}
	}

	public static Path getUserDir(String action, JFrame frame) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(action + " file!");

		if (fc.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(frame, "Didn't " + action.toLowerCase() + " anything!", "Info!", JOptionPane.ERROR_MESSAGE);
			return null;
		}

		return fc.getSelectedFile().toPath();
	}

}