package hr.fer.zemris.ooup.lab4.actions;

import hr.fer.zemris.ooup.lab4.FileUtil;
import hr.fer.zemris.ooup.lab4.model.DocumentModel;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public class SaveAction extends AbstractAction {

	private DocumentModel documentModel;
	private JFrame frame;

	public SaveAction(String name, DocumentModel documentModel, JFrame frame) {
		super(name);

		this.documentModel = documentModel;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Path filePath = FileUtil.getUserDir("Save", frame);
		if (filePath == null) {
			return;
		}

		List<String> rows = new LinkedList<>();
		for (GraphicalObject graphicalObject : documentModel.list()) {
			graphicalObject.save(rows);
		}

		FileUtil.writeLinesToFile(rows, filePath);
	}
}