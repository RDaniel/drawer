package hr.fer.zemris.ooup.lab4.actions;

import hr.fer.zemris.ooup.lab4.FileUtil;
import hr.fer.zemris.ooup.lab4.model.DocumentModel;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;
import hr.fer.zemris.ooup.lab4.renderer.SVGRendererImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.nio.file.Path;

public class SVGAction extends AbstractAction {

	private DocumentModel documentModel;
	private JFrame frame;

	public SVGAction(String name, DocumentModel documentModel, JFrame frame) {
		super(name);

		this.documentModel = documentModel;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Path filePath = FileUtil.getUserDir("SVG", frame);
		if (filePath == null) {
			return;
		}

		SVGRendererImpl r = new SVGRendererImpl(filePath);
		for (GraphicalObject graphicalObject : documentModel.list()) {
			graphicalObject.render(r);
		}
		r.close();
	}
}