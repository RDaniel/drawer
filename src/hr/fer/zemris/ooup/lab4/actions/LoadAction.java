package hr.fer.zemris.ooup.lab4.actions;

import hr.fer.zemris.ooup.lab4.FileUtil;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.CompositeShape;
import hr.fer.zemris.ooup.lab4.model.DocumentModel;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class LoadAction extends AbstractAction {

	private Map<String, GraphicalObject> objectIDMap = new HashMap<>();

	private DocumentModel documentModel;
	private JFrame frame;

	public LoadAction(String name, List<GraphicalObject> objects, DocumentModel documentModel, JFrame frame) {
		super(name);

		this.documentModel = documentModel;
		this.frame = frame;

		fillMap(objects);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Path filePath = FileUtil.getUserDir("Load", frame);
		if (filePath == null) {
			return;
		}

		Stack<GraphicalObject> graphicalObjects = new Stack<>();
		List<String> lines;
		try {
			lines = Files.readAllLines(filePath);
		} catch (IOException ignored) {
			JOptionPane.showMessageDialog(frame, "File " + filePath + " is not readable.", "Error!",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		for (String line : lines) {
			String[] parts = line.split(" ", 2);
			GraphicalObject object = objectIDMap.get(parts[0].trim());
			object.load(graphicalObjects, parts[1].trim());
		}

		for (GraphicalObject graphicalObject : graphicalObjects) {
			documentModel.addGraphicalObject(graphicalObject);
		}
	}

	private void fillMap(List<GraphicalObject> objects) {
		for (GraphicalObject object : objects) {
			objectIDMap.put(object.getShapeID(), object);
		}

		GraphicalObject object = new CompositeShape();
		objectIDMap.put(object.getShapeID(), object);
	}
}
