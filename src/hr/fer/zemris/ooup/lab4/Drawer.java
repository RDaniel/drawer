package hr.fer.zemris.ooup.lab4;

import hr.fer.zemris.ooup.lab4.model.DocumentModel;
import hr.fer.zemris.ooup.lab4.model.DocumentModelListener;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.renderer.G2DRenderer;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class Drawer extends JComponent implements DocumentModelListener {

	private DocumentModel documentModel;

	private StateProvider stateProvider;

	public Drawer(DocumentModel documentModel, StateProvider stateProvider) {
		this.documentModel = documentModel;
		this.stateProvider = stateProvider;

		this.documentModel.addDocumentModelListener(this);

		addMouseListeners();
		addKeyListeners();

		requestFocus();
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Renderer r = new G2DRenderer(g2d);

		documentModel.list().forEach(obj -> {
			obj.render(r);
			stateProvider.getCurrentState().afterDraw(r, obj);
		});

		stateProvider.getCurrentState().afterDraw(r);
	}

	@Override
	public void documentChange() {
		repaint();
	}

	private void addMouseListeners() {
		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				requestFocus();

				if (e.getButton() == MouseEvent.BUTTON1) {
					stateProvider.getCurrentState().mouseDown(new Point(e.getX(), e.getY()),
							(e.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) != 0,
							(e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					stateProvider.getCurrentState().mouseUp(new Point(e.getX(), e.getY()),
							(e.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) != 0,
							(e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0);
				}
			}

		});

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				stateProvider.getCurrentState().mouseDragged(new Point(e.getX(), e.getY()));
			}
		});
	}

	private void addKeyListeners() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				stateProvider.getCurrentState().keyPressed(e.getKeyCode());
			}
		});
	}
}