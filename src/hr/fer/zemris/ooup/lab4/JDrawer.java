package hr.fer.zemris.ooup.lab4;

import hr.fer.zemris.ooup.lab4.actions.LoadAction;
import hr.fer.zemris.ooup.lab4.actions.SVGAction;
import hr.fer.zemris.ooup.lab4.actions.SaveAction;
import hr.fer.zemris.ooup.lab4.model.*;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.LineSegment;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.Oval;
import hr.fer.zemris.ooup.lab4.state.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class JDrawer extends JFrame implements StateProvider {

	private DocumentModel documentModel;

	private IdleState idleState = new IdleState();

	private State currentState = idleState;

	private List<GraphicalObject> objects;

	public JDrawer(List<GraphicalObject> objects) {
		setTitle("JDrawer");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(1300, 800);
		setLocation(100, 100);
		setVisible(true);

		this.objects = objects;
		documentModel = new DocumentModel(objects);

		initGUI();
	}

	@Override
	public State getCurrentState() {
		return currentState;
	}

	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());

		JToolBar toolbar = createToolbar();

		Drawer drawer = new Drawer(documentModel, this);

		cp.add(drawer, BorderLayout.CENTER);
		cp.add(toolbar, BorderLayout.PAGE_START);

		addKeyListeners();
	}

	private JToolBar createToolbar() {
		JToolBar toolBar = new JToolBar();

		toolBar.add(new JButton(new LoadAction("Učitaj", objects, documentModel, this)));
		toolBar.add(new JButton(new SaveAction("Pohrani", documentModel, this)));
		toolBar.add(new JButton(new SVGAction("SVG Export", documentModel, this)));

		for (GraphicalObject object : objects) {
			toolBar.add(new JButton(new AbstractAction(object.getShapeName()) {

				State state = new AddShapeState(documentModel, object);

				@Override
				public void actionPerformed(ActionEvent e) {
					currentState.onLeaving();
					currentState = state;
				}

			}));
		}

		toolBar.add(new JButton(new AbstractAction("Selektiraj") {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentState.onLeaving();
				currentState = new SelectedShapeState(documentModel);
			}
		}));

		toolBar.add(new JButton(new AbstractAction("Brisanje") {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentState.onLeaving();
				currentState = new EraserState(documentModel);
			}
		}));

		return toolBar;
	}

	private void addKeyListeners() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					currentState.onLeaving();
					currentState = idleState;
				}
			}
		});
	}

	public static void main(String[] args) {
		List<GraphicalObject> objects = new ArrayList<>();

		objects.add(new LineSegment(new Point(20, 40), new Point(50, 150)));
		objects.add(new Oval(new Point(300, 200), new Point(250, 250)));

		SwingUtilities.invokeLater(() -> new JDrawer(objects));
	}
}