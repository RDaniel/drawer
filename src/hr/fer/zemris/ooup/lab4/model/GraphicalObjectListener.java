package hr.fer.zemris.ooup.lab4.model;

import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;

public interface GraphicalObjectListener {

	void graphicalObjectChanged(GraphicalObject go);

	void graphicalObjectSelectionChanged(GraphicalObject go);

}