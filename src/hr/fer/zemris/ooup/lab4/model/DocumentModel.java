package hr.fer.zemris.ooup.lab4.model;

import hr.fer.zemris.ooup.lab4.GeometryUtil;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;

import java.util.*;

public class DocumentModel {

	private final static double SELECTION_PROXIMITY = 10;

	private List<GraphicalObject> objects = new ArrayList<>();

	private List<GraphicalObject> roObjects = Collections.unmodifiableList(objects);

	private List<DocumentModelListener> listeners = new ArrayList<>();

	private List<GraphicalObject> selectedObjects = new ArrayList<>();

	private List<GraphicalObject> roSelectedObjects = Collections.unmodifiableList(selectedObjects);

	private final GraphicalObjectListener goListener = new GraphicalObjectListener() {

		@Override
		public void graphicalObjectChanged(GraphicalObject go) {
			notifyListeners();
		}

		@Override
		public void graphicalObjectSelectionChanged(GraphicalObject go) {
			if (go.isSelected() && !selectedObjects.contains(go)) {
				selectedObjects.add(go);
			} else if (!go.isSelected()) {
				selectedObjects.remove(go);
			}

			notifyListeners();
		}
	};

	public DocumentModel() {
	}

	public DocumentModel(List<GraphicalObject> objects) {
		objects.forEach(this::addGraphicalObject);
	}

	public void clear() {
		objects.forEach(obj -> obj.removeGraphicalObjectListener(goListener));
		objects.clear();
		selectedObjects.clear();

		notifyListeners();
	}

	public void addGraphicalObject(GraphicalObject obj) {
		obj.addGraphicalObjectListener(goListener);

		objects.add(obj);
		if (obj.isSelected()) {
			selectedObjects.add(obj);
		}

		notifyListeners();
	}

	public void removeGraphicalObject(GraphicalObject obj) {
		obj.removeGraphicalObjectListener(goListener);

		objects.remove(obj);
		selectedObjects.remove(obj);

		notifyListeners();
	}

	public List<GraphicalObject> list() {
		return roObjects;
	}

	public void addDocumentModelListener(DocumentModelListener l) {
		listeners.add(l);
	}

	public void removeDocumentModelListener(DocumentModelListener l) {
		listeners.remove(l);
	}

	public void notifyListeners() {
		listeners.forEach(DocumentModelListener::documentChange);
	}

	public List<GraphicalObject> getSelectedObjects() {
		return roSelectedObjects;
	}

	public void increaseZ(GraphicalObject go) {
		int index = objects.indexOf(go);

		if (index == objects.size() - 1) {
			return;
		}

		GraphicalObject next = objects.get(index + 1);

		objects.remove(next);
		objects.remove(go);

		objects.add(index, next);
		objects.add(index + 1, go);

		notifyListeners();
	}

	public void decreaseZ(GraphicalObject go) {
		int index = objects.indexOf(go);

		if (index == 0) {
			return;
		}

		GraphicalObject previous = objects.get(index - 1);

		objects.remove(previous);
		objects.remove(go);

		objects.add(index - 1, go);
		objects.add(index, previous);

		notifyListeners();
	}

	public GraphicalObject findSelectedGraphicalObject(Point mousePoint) {
		double minimalDistance = SELECTION_PROXIMITY;
		GraphicalObject bestFitObject = null;

		for (GraphicalObject object : objects) {
			double distance = object.selectionDistance(mousePoint);
			if (distance < minimalDistance) {
				minimalDistance = distance;
				bestFitObject = object;
			}
		}

		return bestFitObject;
	}

	public int findSelectedHotPoint(GraphicalObject object, Point mousePoint) {
		double minDistance = SELECTION_PROXIMITY;
		int hotPointIndex = -1;

		if (object.getNumberOfHotPoints() == 0) {
			return -1;
		}

		for (int i = 0, j = object.getNumberOfHotPoints(); i < j; i++) {
			double distance = GeometryUtil.distanceFromPoint(object.getHotPoint(i), mousePoint);

			if (distance < minDistance) {
				minDistance = distance;
				hotPointIndex = i;
			}
		}

		return hotPointIndex;
	}
}