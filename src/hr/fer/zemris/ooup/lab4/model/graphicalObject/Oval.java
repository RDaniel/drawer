package hr.fer.zemris.ooup.lab4.model.graphicalObject;

import hr.fer.zemris.ooup.lab4.GeometryUtil;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.Rectangle;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Oval extends AbstractGraphicalObject {

	private static final String NAME = "Oval";
	private static final String ID = "@OVAL";

	private Point center;

	public Oval() {
		this(new Point(10, 0), new Point(0, 10));
	}

	public Oval(Point start, Point end) {
		super(new Point[]{start, end});

		center = new Point(end.getX(), start.getY());
	}

	@Override
	public void setHotPoint(int index, Point point) {
		if (index == 0) {
			point = new Point(point.getX(), hotPoints[0].getY());
		} else {
			point = new Point(hotPoints[1].getX(), point.getY());
		}

		super.setHotPoint(index, point);
	}

	@Override
	public void translate(Point point) {
		super.translate(point);
		center = new Point(hotPoints[1].getX(), hotPoints[0].getY());
	}

	@Override
	public Point getTranslationPoint() {
		return center;
	}

	@Override
	public Rectangle getBoundingBox() {
		int x = center.getX() - Math.abs((hotPoints[0].getX() - center.getX()));
		int y = hotPoints[1].getY() < center.getY() ? hotPoints[1].getY() : center.getY() - (hotPoints[1].getY() - center.getY());

		int width = Math.abs(center.getX() - x) * 2;
		int height = Math.abs(center.getY() - y) * 2;

		return new Rectangle(x, y, width, height);
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		int a = Math.abs(hotPoints[0].getX() - center.getX());
		int b = Math.abs(center.getY() - hotPoints[1].getY());

		int x = mousePoint.getX() - center.getX();
		int y = mousePoint.getY() - center.getY();
		double equation = x * x / (double) (a * a) + y * y / (double) (b * b);
		if (equation <= 1.1) {
			return 0;
		}

		return GeometryUtil.distanceFromPoint(mousePoint, center) - Math.min(a, b);
	}

	@Override
	public String getShapeName() {
		return NAME;
	}

	@Override
	public String getShapeID() {
		return ID;
	}

	@Override
	public GraphicalObject duplicate() {
		Oval oval = new Oval(hotPoints[0].duplicate(), hotPoints[1].duplicate());
		oval.hotPointsSelected = Arrays.copyOf(hotPointsSelected, hotPointsSelected.length);
		oval.selected = selected;

		return oval;
	}

	@Override
	public void render(Renderer r) {
		int a = Math.abs(hotPoints[0].getX() - center.getX());
		int b = Math.abs(center.getY() - hotPoints[1].getY());

		Point[] points = new Point[a * 2 * 2 + 2];

		int counter = 0;
		for (int x = -a, j = center.getX() + a; x + center.getX() <= j; x++) {
			int y1 = center.getY() + (int) Math.round(Math.sqrt(b * b - b * b / (double) (a * a) * x * x));
			int y2 = center.getY() - (int) Math.round(Math.sqrt(b * b - b * b / (double) (a * a) * x * x));

			points[counter++] = new Point(center.getX() + x, y1);
			points[points.length - counter] = new Point(center.getX() + x, y2);
		}


		r.fillPolygon(points);
	}

	@Override
	public void save(List<String> rows) {
		String ovalDescription = ID + " " +
				hotPoints[0].getX() + " " + hotPoints[0].getY() + " " +
				hotPoints[1].getX() + " " + hotPoints[1].getY();

		rows.add(ovalDescription);
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		String[] parts = data.split("\\s+");
		stack.push(new Oval(new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])),
				new Point(Integer.parseInt(parts[2]), Integer.parseInt(parts[3]))));
	}
}