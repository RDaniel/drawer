package hr.fer.zemris.ooup.lab4.model.graphicalObject;

import hr.fer.zemris.ooup.lab4.model.GraphicalObjectListener;
import hr.fer.zemris.ooup.lab4.model.Point;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGraphicalObject implements GraphicalObject {

	protected Point[] hotPoints;

	protected boolean[] hotPointsSelected;

	protected boolean selected;

	private List<GraphicalObjectListener> listeners = new ArrayList<>();

	public AbstractGraphicalObject() {
		this(new Point[0]);
	}

	public AbstractGraphicalObject(Point[] hotPoints) {
		this.hotPoints = hotPoints;
		hotPointsSelected = new boolean[hotPoints.length];
	}

	@Override
	public Point getHotPoint(int index) {
		return hotPoints[index];
	}

	@Override
	public void setHotPoint(int index, Point point) {
		hotPoints[index] = point;

		notifyListeners();
	}

	@Override
	public int getNumberOfHotPoints() {
		return hotPoints.length;
	}

	@Override
	public double getHotPointDistance(int index, Point point) {
		return 0.0;
	}

	@Override
	public boolean isHotPointSelected(int index) {
		return hotPointsSelected[index];
	}

	@Override
	public void setHotPointSelected(int index, boolean selected) {
		hotPointsSelected[index] = selected;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(boolean selected) {
		if(this.selected == selected) {
			return;
		}

		this.selected = selected;

		notifySelectionListeners();
	}

	@Override
	public void translate(Point point) {
		for (int i = 0; i < hotPoints.length; i++) {
			hotPoints[i] = hotPoints[i].translate(point);
		}

		notifyListeners();
	}

	@Override
	public void addGraphicalObjectListener(GraphicalObjectListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeGraphicalObjectListener(GraphicalObjectListener listener) {
		listeners.remove(listener);
	}


	private void notifyListeners() {
		listeners.forEach(listener -> listener.graphicalObjectChanged(this));
	}

	private void notifySelectionListeners() {
		listeners.forEach(listener -> listener.graphicalObjectSelectionChanged(this));
	}
}