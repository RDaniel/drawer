package hr.fer.zemris.ooup.lab4.model.graphicalObject;

import hr.fer.zemris.ooup.lab4.GeometryUtil;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.Rectangle;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class LineSegment extends AbstractGraphicalObject {

	private static final String NAME = "Linija";
	private static final String ID = "@LINE";

	public LineSegment() {
		this(new Point(0, 0), new Point(10, 0));
	}

	public LineSegment(Point start, Point end) {
		super(new Point[]{start, end});
	}

	@Override
	public Rectangle getBoundingBox() {
		Point start = hotPoints[0];
		Point end = hotPoints[1];

		int x = start.getX() < end.getX() ? start.getX() : end.getX();
		int width = Math.abs(start.getX() - end.getX());

		int y = start.getY() < end.getY() ? start.getY() : end.getY();
		int height = Math.abs(start.getY() - end.getY());

		return new Rectangle(x, y, width, height);
	}

	@Override
	public Point getTranslationPoint() {
		return hotPoints[0];
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		return GeometryUtil.distanceFromLineSegment(hotPoints[0], hotPoints[1], mousePoint);
	}

	@Override
	public String getShapeName() {
		return NAME;
	}

	@Override
	public String getShapeID() {
		return ID;
	}

	@Override
	public GraphicalObject duplicate() {
		LineSegment segment = new LineSegment(hotPoints[0].duplicate(), hotPoints[1].duplicate());
		segment.hotPointsSelected = Arrays.copyOf(hotPointsSelected, hotPointsSelected.length);
		segment.selected = selected;

		return segment;
	}

	@Override
	public void render(Renderer r) {
		r.drawLine(hotPoints[0], hotPoints[1]);
	}

	@Override
	public void save(List<String> rows) {
		String lineDescription = ID + " " +
				hotPoints[0].getX() + " " + hotPoints[0].getY() + " " +
				hotPoints[1].getX() + " " + hotPoints[1].getY();

		rows.add(lineDescription);
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		String[] parts = data.split("\\s+");
		stack.push(new LineSegment(new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])),
				new Point(Integer.parseInt(parts[2]), Integer.parseInt(parts[3]))));
	}
}