package hr.fer.zemris.ooup.lab4.model.graphicalObject;

import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.Rectangle;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class CompositeShape extends AbstractGraphicalObject {

	private static final String NAME = "Composite";
	private static final String ID = "@COMP";

	List<GraphicalObject> children = new ArrayList<>();

	public CompositeShape() {

	}

	public CompositeShape(List<GraphicalObject> children) {
		this.children.addAll(children);
	}

	public List<GraphicalObject> getChildren() {
		return children;
	}

	@Override
	public Point getTranslationPoint() {
		return children.get(0).getHotPoint(0);
	}

	@Override
	public Rectangle getBoundingBox() {
		Rectangle boundingBox = children.get(0).getBoundingBox();
		int minX = boundingBox.getX();
		int minY = boundingBox.getY();
		int maxX = boundingBox.getX() + boundingBox.getWidth();
		int maxY = boundingBox.getY() + boundingBox.getHeight();

		for (int i = 1, j = children.size(); i < j; i++) {
			boundingBox = children.get(i).getBoundingBox();
			if (boundingBox.getX() < minX) {
				minX = boundingBox.getX();
			}
			if (boundingBox.getY() < minY) {
				minY = boundingBox.getY();
			}
			if (boundingBox.getX() + boundingBox.getWidth() > maxX) {
				maxX = boundingBox.getX() + boundingBox.getWidth();
			}
			if (boundingBox.getY() + boundingBox.getHeight() > maxY) {
				maxY = boundingBox.getY() + boundingBox.getHeight();
			}
		}

		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	@Override
	public void translate(Point point) {
		children.forEach(child -> child.translate(point));
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		double minDistance = children.get(0).selectionDistance(mousePoint);

		for (int i = 1, j = children.size(); i < j; i++) {
			double childDistance = children.get(i).selectionDistance(mousePoint);
			if (childDistance < minDistance) {
				minDistance = childDistance;
			}
		}

		return minDistance;
	}

	@Override
	public String getShapeName() {
		return NAME;
	}

	@Override
	public String getShapeID() {
		return ID;
	}

	@Override
	public GraphicalObject duplicate() {
		List<GraphicalObject> graphicalObjects = new ArrayList<>();
		for (GraphicalObject child : children) {
			graphicalObjects.add(child.duplicate());
		}

		return new CompositeShape(graphicalObjects);
	}

	@Override
	public void render(Renderer r) {
		for (GraphicalObject child : children) {
			child.render(r);
		}
	}

	@Override
	public void save(List<String> rows) {
		children.forEach(child -> child.save(rows));
		rows.add(ID + " " + children.size());
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		int counter = Integer.parseInt(data);
		List<GraphicalObject> children = new LinkedList<>();

		while (counter-- > 0) {
			children.add(stack.pop());
		}

		stack.push(new CompositeShape(children));
	}
}