package hr.fer.zemris.ooup.lab4.model.graphicalObject;

import hr.fer.zemris.ooup.lab4.model.GraphicalObjectListener;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.Rectangle;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import java.util.List;
import java.util.Stack;

public interface GraphicalObject {

	// Podrška za uređivanje objekta
	boolean isSelected();

	void setSelected(boolean selected);

	int getNumberOfHotPoints();

	Point getHotPoint(int index);

	Point getTranslationPoint();

	void setHotPoint(int index, Point point);

	boolean isHotPointSelected(int index);

	void setHotPointSelected(int index, boolean selected);

	double getHotPointDistance(int index, Point mousePoint);

	// Geometrijska operacija nad oblikom
	void translate(Point delta);

	Rectangle getBoundingBox();

	double selectionDistance(Point mousePoint);

	// Observer za dojavu promjena modelu
	public void addGraphicalObjectListener(GraphicalObjectListener l);

	public void removeGraphicalObjectListener(GraphicalObjectListener l);

	// Podrška za prototip (alatna traka, stvaranje objekata u crtežu, ...)
	String getShapeName();

	GraphicalObject duplicate();

	void render(Renderer r);

	public String getShapeID();

	public void save(List<String> rows);

	public void load(Stack<GraphicalObject> stack, String data);
}