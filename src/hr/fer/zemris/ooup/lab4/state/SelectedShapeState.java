package hr.fer.zemris.ooup.lab4.state;

import hr.fer.zemris.ooup.lab4.model.*;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.Rectangle;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.CompositeShape;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class SelectedShapeState extends StateAdapter {

	private static final int NUM_OF_RECTANGLE_POINTS = 4;
	private static final int HOTBOX_HALF_WIDTH = 4;

	private DocumentModel documentModel;

	private GraphicalObject graphicalObject;
	private int selectedHotPoint;

	public SelectedShapeState(DocumentModel documentModel) {
		this.documentModel = documentModel;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		if (!ctrlDown) {
			List<GraphicalObject> selectedObjects = new ArrayList<>(documentModel.getSelectedObjects());
			selectedObjects.forEach(obj -> obj.setSelected(false));
		}

		graphicalObject = documentModel.findSelectedGraphicalObject(mousePoint);
		if (graphicalObject == null) {
			return;
		}

		selectedHotPoint = documentModel.findSelectedHotPoint(graphicalObject, mousePoint);

		graphicalObject.setSelected(true);
	}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		graphicalObject = null;
	}

	@Override
	public void mouseDragged(Point mousePoint) {
		if (graphicalObject != null && selectedHotPoint != -1) {
			graphicalObject.setHotPoint(selectedHotPoint, mousePoint.duplicate());
		}
	}

	@Override
	public void keyPressed(int keyCode) {
		switch (keyCode) {
			case KeyEvent.VK_UP:
				transformSelected(new Point(0, -1));
				break;
			case KeyEvent.VK_DOWN:
				transformSelected(new Point(0, 1));
				break;
			case KeyEvent.VK_RIGHT:
				transformSelected(new Point(1, 0));
				break;
			case KeyEvent.VK_LEFT:
				transformSelected(new Point(-1, 0));
				break;
			case KeyEvent.VK_PLUS:
				documentModel.getSelectedObjects().forEach(obj -> documentModel.increaseZ(obj));
				break;
			case KeyEvent.VK_MINUS:
				documentModel.getSelectedObjects().forEach(obj -> documentModel.decreaseZ(obj));
				break;
			case KeyEvent.VK_G:
				createComposite();
				break;
			case KeyEvent.VK_U:
				removeComposite();
		}
	}

	@Override
	public void afterDraw(Renderer r, GraphicalObject go) {
		if (!go.isSelected()) {
			return;
		}

		Rectangle boundingBox = go.getBoundingBox();

		Point[] points = new Point[NUM_OF_RECTANGLE_POINTS];
		points[0] = new Point(boundingBox.getX(), boundingBox.getY());
		points[1] = new Point(boundingBox.getX() + boundingBox.getWidth(), boundingBox.getY());
		points[2] = new Point(boundingBox.getX() + boundingBox.getWidth(), boundingBox.getY() + boundingBox.getHeight());
		points[3] = new Point(boundingBox.getX(), boundingBox.getY() + boundingBox.getHeight());

		for (int i = 0, j = 1; i < points.length; i++, j++) {
			if (j == points.length) {
				j = 0;
			}

			r.drawLine(points[i], points[j]);
		}
	}

	@Override
	public void afterDraw(Renderer r) {
		List<GraphicalObject> selectedObjects = documentModel.getSelectedObjects();
		if (selectedObjects.size() != 1) {
			return;
		}

		GraphicalObject go = selectedObjects.get(0);

		for (int i = 0, j = go.getNumberOfHotPoints(); i < j; i++) {
			Point hotPoint = go.getHotPoint(i);
			int x = hotPoint.getX();
			int y = hotPoint.getY();

			Point[] points = new Point[NUM_OF_RECTANGLE_POINTS];
			points[0] = new Point(x - HOTBOX_HALF_WIDTH, y - HOTBOX_HALF_WIDTH);
			points[1] = new Point(x + HOTBOX_HALF_WIDTH, y - HOTBOX_HALF_WIDTH);
			points[2] = new Point(x + HOTBOX_HALF_WIDTH, y + HOTBOX_HALF_WIDTH);
			points[3] = new Point(x - HOTBOX_HALF_WIDTH, y + HOTBOX_HALF_WIDTH);

			r.drawLine(points[0], points[1]);
			r.drawLine(points[1], points[2]);
			r.drawLine(points[2], points[3]);
			r.drawLine(points[3], points[0]);
		}

		super.afterDraw(r);
	}

	@Override
	public void onLeaving() {
		List<GraphicalObject> selectedObjects = new ArrayList<>(documentModel.getSelectedObjects());
		selectedObjects.forEach(obj -> obj.setSelected(false));
	}

	private void transformSelected(Point transformPoint) {
		List<GraphicalObject> selectedObjects = documentModel.getSelectedObjects();
		selectedObjects.forEach(obj -> obj.translate(transformPoint));
	}

	private void removeComposite() {
		List<GraphicalObject> objects = new ArrayList<>(documentModel.getSelectedObjects());
		if (objects.size() != 1 || !(objects.get(0) instanceof CompositeShape)) {
			return;
		}

		CompositeShape compositeShape = (CompositeShape) objects.get(0);
		documentModel.removeGraphicalObject(compositeShape);

		List<GraphicalObject> children = compositeShape.getChildren();
		children.forEach(documentModel::addGraphicalObject);
	}

	private void createComposite() {
		List<GraphicalObject> objects = new ArrayList<>(documentModel.getSelectedObjects());
		objects.forEach(documentModel::removeGraphicalObject);

		GraphicalObject compositeObject = new CompositeShape(objects);
		documentModel.addGraphicalObject(compositeObject);
		compositeObject.setSelected(true);
	}
}