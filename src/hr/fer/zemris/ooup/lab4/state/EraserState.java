package hr.fer.zemris.ooup.lab4.state;

import hr.fer.zemris.ooup.lab4.model.DocumentModel;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

import java.util.ArrayList;
import java.util.List;

public class EraserState extends StateAdapter {

	private DocumentModel documentModel;

	private List<GraphicalObject> graphicalObjects = new ArrayList<>();

	private List<Point> linePoints = new ArrayList<>();

	public EraserState(DocumentModel documentModel) {
		this.documentModel = documentModel;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
	}

	@Override
	public void mouseDragged(Point mousePoint) {
		linePoints.add(mousePoint);

		GraphicalObject graphicalObject = documentModel.findSelectedGraphicalObject(mousePoint);
		if (graphicalObject != null) {
			graphicalObjects.add(graphicalObject);
		}

		documentModel.notifyListeners();
	}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		for (GraphicalObject graphicalObject : graphicalObjects) {
			documentModel.removeGraphicalObject(graphicalObject);
		}

		graphicalObjects.clear();
		linePoints.clear();
	}

	@Override
	public void afterDraw(Renderer r) {
		for (int i = 0; i < linePoints.size() - 1; i++) {
			r.drawLine(linePoints.get(i), linePoints.get(i + 1));
		}
	}
}