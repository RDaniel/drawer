package hr.fer.zemris.ooup.lab4.state;

import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.renderer.Renderer;

public class StateAdapter implements State {

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {

	}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {

	}

	@Override
	public void mouseDragged(Point mousePoint) {

	}

	@Override
	public void keyPressed(int keyCode) {

	}

	@Override
	public void afterDraw(Renderer r, GraphicalObject go) {

	}

	@Override
	public void afterDraw(Renderer r) {

	}

	@Override
	public void onLeaving() {

	}

}