package hr.fer.zemris.ooup.lab4.state;

import hr.fer.zemris.ooup.lab4.model.DocumentModel;
import hr.fer.zemris.ooup.lab4.model.graphicalObject.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;

public class AddShapeState extends StateAdapter {

	private DocumentModel model;
	private GraphicalObject prototype;

	public AddShapeState(DocumentModel model, GraphicalObject prototype) {
		this.model = model;
		this.prototype = prototype;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		GraphicalObject graphicalObject = prototype.duplicate();
		Point firstPoint = graphicalObject.getTranslationPoint();

		int dx = mousePoint.getX() - firstPoint.getX();
		int dy = mousePoint.getY() - firstPoint.getY();

		graphicalObject.translate(new Point(dx, dy));

		model.addGraphicalObject(graphicalObject);
	}
}