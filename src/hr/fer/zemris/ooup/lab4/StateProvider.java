package hr.fer.zemris.ooup.lab4;

import hr.fer.zemris.ooup.lab4.state.State;

public interface StateProvider {

	State getCurrentState();

}