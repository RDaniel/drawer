package hr.fer.zemris.ooup.lab4.renderer;

import hr.fer.zemris.ooup.lab4.FileUtil;
import hr.fer.zemris.ooup.lab4.model.Point;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class SVGRendererImpl implements Renderer {

	private List<String> lines = new ArrayList<>();
	private Path filePath;

	public SVGRendererImpl(Path filePath) {
		this.filePath = filePath;
		lines.add("<svg xmlns=\"http://www.w3.org/2000/svg\"");
		lines.add("xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
	}

	public void close() {
		lines.add("</svg>");
		FileUtil.writeLinesToFile(lines, filePath);
	}

	@Override
	public void drawLine(Point s, Point e) {
		lines.add("<line x1=\"" + s.getX() + "\" y1=\"" + s.getY() +
				"\"  x2=\"" + e.getX() + "\" y2=\"" + e.getY() +
				"\" style=\"stroke:#0000ff;\"/>");
	}

	@Override
	public void fillPolygon(Point[] points) {
		StringBuilder pointsBuilder = new StringBuilder();
		for (Point point : points) {
			pointsBuilder.append(point.getX()).append(",").append(point.getY()).append(" ");
		}

		lines.add("<polyline points=\"" + pointsBuilder.toString() + "\" style=\"stroke:#ff0000; fill: #0000ff\"/>");
	}
}