package hr.fer.zemris.ooup.lab4.renderer;

import hr.fer.zemris.ooup.lab4.model.Point;

import java.awt.*;

public class G2DRenderer implements Renderer {

	private Graphics2D g2d;

	public G2DRenderer(Graphics2D g2d) {
		this.g2d = g2d;
	}

	@Override
	public void drawLine(Point s, Point e) {
		g2d.setColor(Color.BLUE);
		g2d.drawLine(s.getX(), s.getY(), e.getX(), e.getY());
	}

	@Override
	public void fillPolygon(Point[] points) {
		g2d.setColor(Color.BLUE);

		int[] xValues = new int[points.length];
		int[] yValues = new int[points.length];
		for (int i = 0; i < points.length; i++) {
			xValues[i] = points[i].getX();
			yValues[i] = points[i].getY();
		}

		g2d.fillPolygon(xValues, yValues, points.length);

		g2d.setColor(Color.RED);
		g2d.drawPolygon(xValues, yValues, points.length);
	}
}